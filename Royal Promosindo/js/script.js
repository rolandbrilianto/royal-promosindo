//Kode untuk memunculkan menu di kanan atas
var navLinks = document.getElementById("navLinks");
//funsi javascript 1 kode untuk memunculkan side menu
function showMenu() {
  navLinks.style.right = "0";
}
//fungsi javascript 2 kode untuk menghilangkan menu
function hideMenu() {
  navLinks.style.right = "-200px";
}

//slideshow

var slideIndex = 1;
showSlides(slideIndex);

//fungsi javascript 3 kode untuk memindahkan gambar
function plusSlides(n) {
  showSlides((slideIndex += n));
}

//fungsi javascript 4 kode untuk mengontrol gambarnya
function currentSlide(n) {
  showSlides((slideIndex = n));
}

//fungsi javascript 5 kode untuk mekanisme pemindahan slideshow
function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {
    slideIndex = 1;
  }
  if (n < 1) {
    slideIndex = slides.length;
  }
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex - 1].style.display = "block";
  dots[slideIndex - 1].className += " active";
}
